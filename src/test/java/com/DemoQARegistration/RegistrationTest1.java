package com.DemoQARegistration; 

import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.demoqa.registration.Demoqa_Registration;
import com.testbase.TestBase;
import com.utility.ScreenshotsUtility;

public class RegistrationTest1 extends TestBase {
	
	Demoqa_Registration demoqareg;
	//ScreenshotsUtility screenshot;
	TestBase testbase;
	boolean c5;
	
  @Test
  public void checkRegistration1() throws Exception {
	  
	 String inputValueForFirstName= "Jim";
	 String inputValueForLastName = "";
	 
	 demoqareg = new Demoqa_Registration(driver);
	 demoqareg.clickRegistration();
	 
	 demoqareg.setFirstname(inputValueForFirstName);
	 Thread.sleep(1000);
	 
	 demoqareg.setLastname(inputValueForLastName);
	 Thread.sleep(1000);
	 
	 demoqareg.checkErrorForFirstName(inputValueForFirstName);
	// Thread.sleep(1000);
	 
	 demoqareg.selectMaritalStatus("Single");
	 
	 demoqareg.checkErrorForLastName(inputValueForLastName);
	// Thread.sleep(1000);
	 
	// demoqareg.selectCheckBox("");
	 demoqareg.selectCheckBox("Dance");
	
	 
	 demoqareg.selectCountry();
	// demoqareg.selectCountry("India");
	 Thread.sleep(1000);
	 
	 demoqareg.selectDatepicker("12", "6", "1991");
	 
	 demoqareg.selectPhoneNumber("062253");
	 Thread.sleep(1000);
	 
	 demoqareg.setUsername("jimclarry");
	 demoqareg.setEmailAddress("jimclarry@gmail.com");
	 demoqareg.setProfilePicture("C:\\Users\\evarghes\\Pictures\\COC_Ebila.jpg");
	 demoqareg.setPassword("jim@clarry123");
	 demoqareg.setConfirmPassword("jim@clarry123");
	 //demoqareg.registerToDemoqa("Jim", "Larry");  
  }
  
  @AfterMethod
  public void takeFailedScreenshot(ITestResult result) {
	  
	  if(ITestResult.FAILURE == result.getStatus()) {
		  
	  	  ScreenshotsUtility.captureScreenshot(driver, "ErrorInRegi");
	  }
  }
  
  @BeforeTest
  public void beforeTest() { 
	  init();
  }

  @AfterTest
  public void afterTest() {
	  driver.close();
  }

}
