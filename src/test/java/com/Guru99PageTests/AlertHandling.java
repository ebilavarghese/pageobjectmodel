package com.Guru99PageTests;

import org.testng.annotations.Test;

import com.guru99page.Guru99ObjectRepository;
import com.testbase.TestBase;
import com.utility.ScreenshotsUtility;

import org.testng.annotations.BeforeTest;
import org.openqa.selenium.Alert;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;

public class AlertHandling extends TestBase{
	
	Guru99ObjectRepository guru;
	
  @Test
  public void handleAlertsInGuru99() throws InterruptedException {
	   
	  guru = new Guru99ObjectRepository(driver);
	  guru.deleteCustomer("5698",driver);
	  Thread.sleep(1000);
  }
  
  @AfterMethod
  
  		public void takeFailedScreenshot(ITestResult result) {
	  
	  		if(ITestResult.FAILURE == result.getStatus()) {
		  
	  			ScreenshotsUtility.captureScreenshot(driver, "ErrorInRegi");
	  		}
  		}
  @BeforeTest
  public void beforeTest() {
	  init();
  }

  @AfterTest
  public void afterTest() {
	  driver.close();
  }

}
