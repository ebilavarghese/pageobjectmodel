package com.testbase;

import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;

public class TestBase {
	
	public static final Logger log = Logger.getLogger(TestBase.class.getName());
	
	public WebDriver driver;
	//String url = "https://www.flipkart.com/";
	String browser = "chrome";
	String url = "http://demoqa.com/";
	//String url = "http://demo.guru99.com/test/delete_customer.php";
		//String browser = "IE";
	
	public void init(){
		String log4jConfigPath = "log4j.properties";
		PropertyConfigurator.configure(log4jConfigPath);
		selectBrowser(browser);
		getURL(url);
				//System.out.println("url"+url);	
	}
	
	public void selectBrowser(String browser){
		if(browser.equalsIgnoreCase("chrome")){
			
			System.setProperty("webdriver.chrome.driver", "C:\\Tools\\chromedriver.exe");
			log.info("creating object of browser");
			  driver = new ChromeDriver();
		}
		
		else if(browser.equalsIgnoreCase("IE")) {
			System.setProperty("webdriver.edge.driver", "C:\\Program Files (x86)\\Microsoft Web Driver\\MicrosoftWebDriver.exe");
			driver = new EdgeDriver();
		}
	}

	
	public void getURL(String url){
		log.info("navigating to "+url);
		driver.get(url);
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	}
	
	public void closebrowser(WebDriver driver){
		driver.close();
	}
}
