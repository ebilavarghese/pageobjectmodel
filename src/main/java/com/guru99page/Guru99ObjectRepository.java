package com.guru99page;

import org.openqa.selenium.Alert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class Guru99ObjectRepository {
	
	@FindBy(name = "cusid")
	WebElement customerID;
	
	@FindBy(name = "submit")
	WebElement submitButton;

	public Guru99ObjectRepository(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
	
	public void deleteCustomer(String customer_ID, WebDriver driver) {
		System.out.println("entered the method");
		customerID.sendKeys(customer_ID);
		submitButton.click();
		System.out.println("submit button clicked");
		Alert alert = driver.switchTo().alert();
		alert.accept();
		Alert alert1 = driver.switchTo().alert();
		alert1.dismiss();
	}
	
}
