package com.homepageobjects;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.testbase.TestBase;

public class HomePageObjects {
	public static final Logger log = Logger.getLogger(HomePageObjects.class);
	
	@FindBy(xpath ="//*[@id='container']/div/header/div[1]/div[1]/div/ul/li[9]/a")
	WebElement logIn;
	
	//@FindBy(xpath ="html/body/div[2]/div/div/div/div/div[2]/div/form/div[1]/input")
	@FindBy(className = "_2zrpKA")
	WebElement emailAddress;
	
	//@FindBy(xpath ="html/body/div[2]/div/div/div/div/div[2]/div/form/div[2]/input")
	@FindBy(className = "_2zrpKA _3v41xv")
	WebElement loginPassword;
	
	//@FindBy(xpath ="html/body/div[2]/div/div/div/div/div[2]/div/form/div[3]/button")
	@FindBy(className = "_2AkmmA _1LctnI _7UHT_c")
	WebElement loginButton;
	
	
	public HomePageObjects(WebDriver driver){
		PageFactory.initElements(driver, this);
	}
	
	public void loginToApplication(String email, String password){
		
		//logIn.click();
		emailAddress.sendKeys(email);
		loginPassword.sendKeys(password);
		loginButton.click();
	}
	

}
