package com.demoqa.registration;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

public class Demoqa_Registration {
	
	@FindBy(css = "li#menu-item-374")
	WebElement registrationLink;
	
	@FindBy(id = "name_3_firstname")
	WebElement firstName;
	
	@FindBy(id = "name_3_lastname")
	WebElement lastName;
	
	@FindBy(xpath = "//input[@name='radio_4[]' and @value='single']")
	WebElement maritalStatus_Single;
	
	@FindBy(xpath = "//input[@name='radio_4[]' and @value='married']")
	WebElement maritalStatus_Married;
	
	@FindBy(xpath = "//input[@name='radio_4[]' and @value='divorced']")
	WebElement maritalStatus_Divorced;
	
	@FindBy(xpath = "//input[@name='checkbox_5[]' and @value='dance']")
	WebElement hobby_Dance;
	
	@FindBy(xpath = "//input[@name='checkbox_5[]' and @value='reading']")
	WebElement hobby_Reading;
	
	@FindBy(xpath = "//input[@name='checkbox_5[]' and @value='cricket ']")
	WebElement hobby_Cricket;
	
	@FindBy(id = "dropdown_7")
	WebElement countryDropdown;
	
	
	@FindBy(id = "mm_date_8")
	WebElement month_Dropdown;
	
	
	@FindBy(id = "dd_date_8")
	WebElement date_Dropdown;
	
	
	@FindBy(id = "yy_date_8")
	WebElement year_Dropdown;
	
	
	@FindBy(id = "phone_9")
	WebElement phone_Number;
	
	@FindBy(id = "username")
	WebElement username;
	
	@FindBy(id = "email_1")
	WebElement email;
	
	@FindBy(id ="profile_pic_10")
	WebElement profile_Pic;
	
	@FindBy(id = "description")
	WebElement descriptionBox;
	
	@FindBy(id = "password_2")
	WebElement password2;
	
	@FindBy(id = "confirm_password_password_2")
	WebElement confirmPassword2;
	
	@FindBy(id = "piereg_passwordStrength")
	WebElement passwordStrength;
	
	@FindBy(id = "pie_submit")
	WebElement submitButton;
	
	//label[contains(., 'labelName')]/parent::*//input[@value='elementToSelect'] 
	
	

	@FindBy(xpath = "//*[@id='pie_register']/li[1]/div[1]/div[2]/span")
	//@FindBy(xpath ="//*[@class='legend error']")
	//@FindBy(xpath = "//span[@class='legend error']/ancestor::*//input[@id = 'name_3_firstname']")
	//@FindBy(css = "span.legend.error")
	WebElement PathToErrorMessage;
	
	@FindBy(xpath = "//*[@id='pie_register']/li[3]/div/div[2]/span")
	//@FindBy(css = "span[class = 'legend error']")
	WebElement checkboxErrorMessage;
	
	@FindBy(xpath = "//*[@id='pie_register']/li[6]/div/div/span")
	WebElement errorInPhoneNumber;
	
	@FindBy(xpath = "//*[@id='pie_register']/li[7]/div/div/span")
	WebElement errorInUsername;
	
	@FindBy(xpath = "//*[@id='pie_register']/li[6]/div/div/span")
	WebElement errorInPhoneLength;
	
	public  Demoqa_Registration(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
	
	public void clickRegistration() {
		registrationLink.click();
	}
	public void setFirstname(String firstname) {
		firstName.sendKeys(firstname);			
					}
		
	public void checkErrorForFirstName(String firstname) {
		
	//	WebElement ErrorMessage=driver.findElement(By.id("pie_register")).findElement(By.xpath("//*[@class='legend error']"));
	//	System.out.println("Error Message      "+ErrorMessage.getText());
		//System.out.println("Printing text "+firstname);
		try {
		
			if (firstname == null || firstname.equals("")) {
				System.out.println("ErrorMessage.isDisplayed() "+PathToErrorMessage.isDisplayed());
					if(PathToErrorMessage.isDisplayed()) {
							System.out.println(PathToErrorMessage.getText());
							Assert.assertEquals(PathToErrorMessage.getText(), "* This field is required");
							new Exception("First Name cannot be left blank!!!!");
					}
			}
		}
		catch(Exception e){
			
				

                     e.printStackTrace();
				}
		
		}
		
		//boolean c = firstName.isDisplayed();
		//Assert.assertEquals(c, false);
		
	
	
	
	public void setLastname(String lastname) {
		lastName.sendKeys(lastname);
	}
	
	public void checkErrorForLastName(String lastname) {
		
		try {
			
			if (lastname == null || lastname.equals("")) {
				//System.out.println("ErrorMessage.isDisplayed() "+PathToErrorMessage.isDisplayed());
					if(PathToErrorMessage.isDisplayed()) {
							//System.out.println(PathToErrorMessage.getText());
							Assert.assertEquals(PathToErrorMessage.getText(), "* This field is required");
							new Exception("First Name cannot be left blank!!!!");
					}
			}
		}
		catch(Exception e){
			
				

                     e.printStackTrace();
				}
		
		}
		
	
	public void selectMaritalStatus(String MaritalStatus) {
		
				
		switch(MaritalStatus){
		 
		case "Single":
			maritalStatus_Single.click();
			break;
			
		case "Married":
			maritalStatus_Married.click();
			break;
			
		case "Divorced":
			maritalStatus_Divorced.click();
			break;
			
			default:
				break;
			
			
		}
	}
	
	public void selectCheckBox(String check) {
		
		switch(check){
		
		case "Dance":
			hobby_Dance.click();
			hobby_Dance.sendKeys(Keys.TAB);
			break;
			
		case "Reading":
			hobby_Reading.click();
			hobby_Reading.sendKeys(Keys.TAB);
			break;
			
		case "Cricket":
			hobby_Cricket.click();
			hobby_Cricket.sendKeys(Keys.TAB);
			break;
			
				
			default:{
	
				
				if (check == null || check.equals("")) {
					System.out.println("Check message"+check);
					//System.out.println("ErrorMessage.isDisplayed() "+PathToErrorMessage.isDisplayed());
						if(checkboxErrorMessage.isDisplayed()) {
								//System.out.println(PathToErrorMessage.getText());
								Assert.assertEquals(checkboxErrorMessage.getText(), "* This field is required");
								new Exception("Enter a valid selection!!!");
								break;
						}
						
						else {
							
							System.out.println("Error Message should be displayed if checkbox is left empty");
							Assert.assertEquals(true, false);
							break;
							
						}
				}
				
				else {
				System.out.println("Not a valid selection   "+check);
				break;
					}
	
			}
		}
	}
	public void selectCountry() {
		Select country = new Select(countryDropdown);
		//country.selectByVisibleText(countryname);
		country.getFirstSelectedOption();
	}
	
	public boolean selectDatepicker(String month1, String date1, String year1) throws Exception {
		Select month = new Select(month_Dropdown);
		month.selectByVisibleText(month1);
	
	
		
		Select date = new Select(date_Dropdown);
		date.selectByVisibleText(date1);
		
		Select year = new Select(year_Dropdown);
		year.selectByVisibleText(year1);
		
		int yearOfBirth = Integer.parseInt(year1);
		
		if(month1 == "" || month1 == "null") {
			throw new Exception("Birth month cannot be null or empty");
		}
		
		if(date1 ==""|| date1 =="null") {
			throw new Exception("Birth date cannot be null or empty");
		}
		
		if(year1 ==""||year1 =="null") {
			throw new Exception("Birth year cannot be empty");
		}

		if(month1 == "4" || month1 =="6" || month1 =="9"|| month1 =="11") {
			if(date1 =="31") {
			
				throw new Exception("Selected month cannot have 31 days");
			}
		}
		
		else if(month1 == "2") {
			
			if(yearOfBirth % 4 ==0) {
				if(date1.equals("30")|| date1.equals("31")) {
					throw new Exception("February in leap year cannot have days more than 29");
				}
				else {
					return true;
				}
			}
			else {
				if(date1.equals("29")||date1.equals("30")||date1.equals("31")) {
					throw new Exception("February cannot have more than 28 days in non-leap years");
				}
				else {
					return true;
				}
			}
		}
		return false;
		
	}
	
	
	
	public void selectPhoneNumber(String phonenum) throws Exception {
		phone_Number.sendKeys(phonenum);
		phone_Number.sendKeys(Keys.TAB);
		
		if(phonenum == "" || phonenum ==null) {
			
			if(errorInPhoneNumber.isDisplayed()) {
				
				System.out.println("Error Message in Phone number"+errorInPhoneNumber.getText());
				Assert.assertEquals(errorInPhoneNumber.getText(), "* This field is required");
			}
			
			else {
				throw new Exception("Error Message should appear when phone number is left empty");
			}
			
		}
		else if(phonenum.length()<10) {
			
			System.out.println("Length of Phone number is    "+phonenum.length());
			
			if(errorInPhoneLength.isDisplayed()) {
				
				Assert.assertEquals(errorInPhoneLength.getText(), "* Minimum 10 Digits starting with Country Code");
			}
			else {
				throw new Exception("Error Message should be displayed. Phone Number cannot have length less than 10");
			}
			

		}
	}
	
	public void setUsername(String user_name) throws Exception {
		username.sendKeys(user_name);
		username.sendKeys(Keys.TAB);
		
		if(user_name == null ||user_name =="") {
			
			if(errorInUsername.isDisplayed()) {
				System.out.println("Error Message in Username   "+errorInUsername.getText());
				Assert.assertEquals(errorInUsername.getText(), "* This field is required");
			}
			
			else {
				throw new Exception("Error message should appear if username field is left empty");
			}
		}
	}
	
	public void setEmailAddress(String email_address) {
		email.sendKeys(email_address);
	}
	
	public void setProfilePicture(String profile_picture) {
		profile_Pic.sendKeys(profile_picture);
	}
	public void setPassword(String pass_word) {
		password2.sendKeys(pass_word);
	}
	
	public void setConfirmPassword(String confirm_password) {
		confirmPassword2.sendKeys(confirm_password);
	}
	
	
	
	}
	